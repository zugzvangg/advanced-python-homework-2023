import cmd
import threading

from queue import Queue

from equipment.turtle_device import TurtleDevice


class TurtleDeviceThread(threading.Thread):
    # TODO(Homework 4)
    def __init__(self):
        super().__init__()
        self.device = TurtleDevice()
        self.queue = Queue()


class NoBlockingTurtleShell(cmd.Cmd):
    intro = 'Welcome to the turtle shell.   Type help or ? to list commands.\n'
    prompt = '(turtle) '
    file = None

    def __init__(self, turtle_thread: TurtleDeviceThread):
        pass  # TODO(Homework 4)

    def do_execute(self, arg):
        pass  # TODO(Homework 4)

    def do_exit(self, arg):
        pass  # TODO(Homework 4)


if __name__ == '__main__':
    turtle_thread = TurtleDeviceThread()
    # TODO(Homework 4: Correct start thread)
    NoBlockingTurtleShell(turtle_thread).cmdloop()