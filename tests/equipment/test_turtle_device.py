import pytest

from equipment.turtle_device import TurtleDevice


def test_setup():
    td = TurtleDevice()  # не падает


@pytest.mark.parametrize(
    ("trait", "value"), [("DEFAULT_MODE", "standard"), ("DEFAULT_ANGLEOFFSET", 0.0)]
)
def test_read(trait, value):
    td = TurtleDevice()
    assert td.read(trait) == value


@pytest.mark.parametrize(
    ("trait", "value_to_pass"),
    [
        ("DEFAULT_ANGLEOFFSET", 1),
        ("DEFAULT_ANGLEOFFSET", 2),
        pytest.param("DEFAULT_ANGLEOFFSET", 3.0, marks=pytest.mark.xfail),
        ("DEFAULT_MODE", "other_mode"),
    ],
)
def test_write(trait, value_to_pass):
    td = TurtleDevice()
    td.write(trait, value_to_pass)
    assert td[trait] == value_to_pass


@pytest.mark.parametrize(
    ("trait", "value_to_pass"),
    [
        ("DEFAULT_ANGLEOFFSET", 10),
        ("DEFAULT_MODE", "other mode"),
    ],
)
def test_invalidate(trait, value_to_pass):
    td = TurtleDevice()
    td.write(trait, value_to_pass)
    assert td[trait] != td.read(trait)
    td.invalidate(trait)  # сносит логическое значение
    assert td[trait] == td.read(trait)


@pytest.mark.parametrize(
    ("action", "args", "expected"),
    [
        ("back", {"distance": 5.0}, (-5.0, 0.0)),
        pytest.param("back", {"distance": 5.0}, (0.0, -5.0), marks=pytest.mark.xfail),
    ],
)
def test_execute(action, args, expected):
    td = TurtleDevice()
    td.execute(action, **args)
    assert td.turtle.pos() == expected
