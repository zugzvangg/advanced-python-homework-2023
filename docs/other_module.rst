other\_module package
=====================

Submodules
----------

other\_module.snth2 module
--------------------------

.. automodule:: other_module.snth2
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: other_module
   :members:
   :undoc-members:
   :show-inheritance:
