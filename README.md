# advanced-python-homework-2023

advanced-python-homework-2023
Дмитрий Базанов М05-319б

# How to build
```
$ poetry install
```

# How to make docs with Sphinx

```
$ cd docs
$ make html
```

# How to run Pytest tests

```
$ export PYTHONPATH=.
$ pytest
```



